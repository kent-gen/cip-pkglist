#!/usr/bin/env python
#
# generate-proposal.py
#
# A helper script to generate a proposal file with the specified format,
# which is required for the package proposal in the CIP PDP.
#
# Usage:
#   $ ./generate-proposal.py
#
# Finally, this script generates proposal.yml which includes
# the required information for the package proposal.
#
# Copyright (c) 2019 TOSHIBA Corporation
#
# SPDX-License-Identifier: Apache-2.0
#

import common
import datetime
import get_pkg_depends as gpd
import count_cve

Q1 = "Enter proposer name: "
Q2 = "Choose the Debian version: "
Q3 = "Enter the source package name: "
Q4 = "Choose the required binary packages: "
Q5 = "Choose the dependency package in the 'or' list: "
Q6 = "Choose one of the virtual package provider: "
Q7 = "Are any of the binary packages used in target rootfs?"
Q8 = "Is the source package is categorized in to the security criteria?"
Q9 = "Enter the number of CVEs found so far: "
Q10 = "Provide the reason for proposing this package: "
Q11 = "Do you want to add another source package? "


def die(text):
    print(common.ERROR_TAG+text)
    exit(1)


def main():
    """
    Main function of the script
    :return:
    """
    apt = common.Apt()
    cve = count_cve.Cve()
    cve.load_cve_data()
    try:
        pr_request_info = common.PDPProposal.ProposalInfo()

        # Input proposer name
        pr_request_info.proposer_name = common.input_text(Q1)

        # Take proposal date as current date
        pr_request_info.proposal_date=str(datetime.date.today().strftime("%Y/%m/%d"))
        print("Proposal date: "+pr_request_info.proposal_date)

        # Input debian version
        pr_request_info.proposed_debian_version = common.input_choose_radio(Q2, common.DEBIAN_CODE_NAMES, 8)
        print("Proposal debian version: "+ pr_request_info.proposed_debian_version)

        if not apt.apt_initialize(pr_request_info.proposed_debian_version):
            del apt
            die("Apt initialize is failed")

        pdp_info = common.PDPInfo(pr_request_info.proposed_debian_version)
        pdp_info.load_pdp()
        gpd.load_prv_sel_pkg_list(pdp_info)

        while True:
            # Input source package name
            deb_src_pkg_name=""
            while True:
                src_pkg_name_input = common.input_text(Q3)
                src_pkg_name, src_pkg_ver, src_pkg_bin_list = apt.apt_cache_get_src_info(src_pkg_name_input)
                if src_pkg_name != src_pkg_name_input:
                    print("Not valid source package!!!")
                    continue
                else:
                    deb_src_pkg_name = src_pkg_name_input
                    break

            if len(src_pkg_bin_list) == 0 :
                del apt
                die("There are no binary packages found to this source package name ")

            # Choose binary package names
            bin_pkg_list = common.input_choose_combo(Q4, src_pkg_bin_list)

            # choose if any 'or' or virtual packages
            deb_bin_pkg_dict=dict()
            for pkg in bin_pkg_list:
                print("")
                print(pkg)
                dp_list_final = gpd.get_pkg_depends(pkg, apt)
                if not len(dp_list_final):
                    print("\tNo Dependencies in package")
                else:
                    for dp in dp_list_final:
                        print("\t-" + dp)

                deb_bin_pkg_dict[pkg] = dp_list_final

            print("\n")
            in_target = common.input_choose_radio(Q7, ['True', 'False'])
            if in_target == "False":
                in_criteria = "-"
                n_cve = "-"
            else:
                in_criteria = common.input_choose_radio(Q8, ['True', 'False'])
                #n_cve = common.input_text(Q9)
                n_cve = cve.count_cve(deb_src_pkg_name)

            reason = common.input_text(Q10)
            src_pkg_info = common.PDPProposal.SrcPkgInfo()
            src_pkg_info.bin_pkg_dict = deb_bin_pkg_dict
            src_pkg_info.in_target = in_target
            src_pkg_info.in_criteria = in_criteria
            src_pkg_info.n_cve = n_cve
            src_pkg_info.reason = reason
            pr_request_info.proposed_src_pkgs[deb_src_pkg_name]=src_pkg_info

            print("\n")
            if common.input_choose_radio(Q11, ['Yes', 'No']) == "No":
                break
    finally:
        del apt

    print("\n")
    print("################################################")
    print("# Final proposal request")
    print("################################################")
    common.PDPProposal().print_req_info(pr_request_info)
    print("################################################")
    filename="proposal.yml"
    common.PDPProposal().save(pr_request_info, filename)
    print("Proposal Saved in file: "+filename)

if __name__ == "__main__":
   main()
