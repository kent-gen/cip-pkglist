#!/usr/bin/env python
#
# count_cve.py
#
# A helper script to count the number of CVEs
# found in the specified Debian packages so far.
# This script checks CVE data in Debian security tracker
# by parsing a json file provided in the security tracker web site.
#
# Usage:
#   $ ./count_cve.py apt bash busybox openssl
#
# Copyright (c) 2019 TOSHIBA Corporation
#
# SPDX-License-Identifier: Apache-2.0
#

import sys
import urllib2, json
DEBIAN_SEC_TRACK_DATA_URL = 'https://security-tracker.debian.org/tracker/data/json'


class Cve:
    def __init__(self):
        self.cve_data_json = dict()

    def load_cve_data(self):
        """
        Load cve data from Debian security tracker "https://security-tracker.debian.org/tracker/"
        :return: True if loaded successfully
        """
        try:
            print("Fetching CVE data...")
            data = urllib2.urlopen(DEBIAN_SEC_TRACK_DATA_URL)
            self.cve_data_json = json.load(data)
            return True
        except Exception as  e:
            print("ERROR: Failed to load CVE data: " + str(e))
        return False

    def count_cve(self, pkg_name):
        """
        Searches CVE vulnerability count in the data
        :return: count (int)
        """
        if not self.cve_data_json or len(self.cve_data_json) == 0:
            print("WARN: CVE data is not loaded")
            return 0

        if pkg_name in self.cve_data_json.keys():
            return len([1 for vuln in self.cve_data_json[pkg_name].keys() if vuln.startswith('CVE-')])
        else:
            return 0


if __name__ == "__main__":

    cve = Cve()
    if not cve.load_cve_data():
        exit(1)

    print('-' * 40)
    print("Package Name = No.of vunerabilities")
    print('-' * 40)
    for pkg in sys.argv[1:]:
        print(pkg + " = " + str(cve.count_cve(pkg)))
    print('-' * 40)
