cip-pkglist
===========

This repository includes Debian package list which are the target of
CIP's long-term maintenance and helper scripts for proposing or
registering such packages.

Preparation
===========

Install packages required by the helper scripts.

    $ sudo apt install python-apt python-pip
    $ pip install pyyaml

Package Proposal
================

Generate your proposal information.

    $ ./generate-proposal.py

Answer the all questions asked by the script,
then you get the proposal file `proposal.yml`.
Please send this file to CIP Core WG to proceed the review process.

Package Registration
====================

After the proposal accepted, we can register the package information
to the CIP maintained package list using `pdp-helper.py`.

    $ ./pdp-helper.py add-proposal proposal.yml

You can check the package information registered in the list
using the same script. In case of Debian 10 buster:

    $ ./pdp-helper.py show buster libssl1.1 busybox
